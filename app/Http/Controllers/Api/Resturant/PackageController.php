<?php

namespace App\Http\Controllers\Api\Resturant;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Resturant_App\Package\StorePackageRequest;
use App\Http\Requests\Api\Resturant_App\Package\UpdatePackageRequest;
use App\Http\Resources\Api\Resturant_App\Package\PackageResource;
use App\Http\Resources\Api\Resturant_App\Resturant\DayResource;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\Day;
use App\Models\Package;
use App\Models\PackageDetails;
use App\Models\PackageImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PackageController extends Controller
{
    use SaveImageTrait, ApiResponseTrait;

    public function index(Request $request)
    {
        $resturant = $request->user('resturant_api');
        $packages = Package::where('resturant_id', $resturant->id)->orderBy('created_at', 'DESC')->paginate(20);
        return $this->apiResponse(PackageResource::collection($packages), 'All Packages of this restaurant', 200);
    }

    public function getDays()
    {
        return $this->apiResponse(DayResource::collection(Day::all()), 'The required days to create or update a package', 200);
    }

    public function store(StorePackageRequest $request)
    {
        $resturant = $request->user('resturant_api');
        $package = Package::create([
            'resturant_id'    => $resturant->id,
            'price'           => $request->price,
            'total_calories'  => $request->total_calories,
            'carbohydrate'    => $request->carbohydrate,
            'protein'         => $request->protein,
            'fats'            => $request->fats,
            'main_image'      => $this->saveImage($request->file('main_image'), 'Images/Packages/MainImages', 400, 400),
            'status'          => $request->status,
            'ar'  => [
                'name'        => $request->name_ar,
                'description' => $request->description_ar,
            ],
            'en'  => [
                'name'        => $request->name_en,
                'description' => $request->description_en,
            ]
        ]);

        $Images = $request->file('images');
        if ($Images) {
            foreach ($Images as $Image) {
                $image_name_DataBase = $this->saveImages($Image, 'Images/Packages/PackageImages', 400, 400);
                PackageImage::create(['package_id' => $package->id, 'image' => $image_name_DataBase]);
            }
        }

        foreach ($request->input('package_details') as $package_day) {
            PackageDetails::create([
                'package_id'    => $package->id,
                'day_id'        => $package_day['day_id'],
                'delivery_time' => $package_day['delivery_time'],
                'ar'  => ['description' => $package_day['description_ar']],
                'en'  => ['description' => $package_day['description_en']]
            ]);
        }

        return $this->apiResponse(new PackageResource($package), 'The package has been created successfully', 200);
    }

    public function update(UpdatePackageRequest $request, $id)
    {
        $resturant = $request->user('resturant_api');
        $package = Package::find($id);
        $package->update([
            'resturant_id'    => $resturant->id,
            'price'           => $request->price,
            'total_calories'  => $request->total_calories,
            'carbohydrate'    => $request->carbohydrate,
            'protein'         => $request->protein,
            'fats'            => $request->fats,
            'main_image'      =>  $this->updateImage($package, $request, 'main_image', 'Images/Packages/MainImages', 400, 400),
            'status'          => $request->status,
            'ar'  => [
                'name'        => $request->name_ar,
                'description' => $request->description_ar,
            ],
            'en'  => [
                'name'        => $request->name_en,
                'description' => $request->description_en,
            ]
        ]);

        foreach ($request->input('package_details') as $package_day) {
            $this->updatePackageDays($package, $package_day);
        }

        return $this->apiResponse(new PackageResource($package), 'The meal has been updated successfully', 200);
    }

    public function destroy($id)
    {
        $package = Package::find($id);
        (File::exists($package->image)) ? File::delete($package->image) : Null;
        foreach ($package->images()->pluck('image') as $Image) {
            (File::exists($Image)) ? File::delete($Image) : Null;
        }
        $package->delete();
        return $this->apiResponse(null, 'The package has been deleted successfully', 200);
    }

    public function updatePackageDays($package, $package_day)
    {
        $update_package_day = PackageDetails::where([['package_id', $package->id], ['day_id', $package_day['day_id']]])->first();
        if ($package_day['deleted_day'] == '1') {
            PackageDetails::updateOrCreate([
                'day_id'        => $package_day['day_id']
            ], [
                'package_id'    => $package->id,
                'day_id'        => $package_day['day_id'],
                'delivery_time' => $package_day['delivery_time'],
                'ar'  => ['description' => $package_day['description_ar']],
                'en'  => ['description' => $package_day['description_en']]
            ]);
        } elseif ($package_day['deleted_day'] == '0') {
            $update_package_day->delete();
        }
    }

    public function changeStatus(Request $request, $id)
    {
        Package::find($id)->update(['status' => $request->status]);
        return $this->apiResponse(null, 'Package status changed successfully', 200);
    }
}
