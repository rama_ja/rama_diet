<?php

namespace App\Http\Controllers\Api\Resturant;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Resturant_App\Extra\StoreExtraRequest;
use App\Http\Requests\Api\Resturant_App\Extra\UpdateExtraRequest;
use App\Http\Resources\Api\Resturant_App\Extra\ExtraResource;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\Extra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ExtraController extends Controller
{
    use SaveImageTrait, ApiResponseTrait;

    public function index(Request $request)
    {
        $resturant = $request->user('resturant_api');
        $meals = Extra::where('resturant_id', $resturant->id)->orderBy('created_at', 'DESC')->paginate(20);
        return $this->apiResponse(ExtraResource::collection($meals), 'All extras of this restaurant', 200);
    }

    public function store(StoreExtraRequest $request)
    {
        $resturant = $request->user('resturant_api');

        $extra = Extra::create([
            'resturant_id'    => $resturant->id,
            'price'           => $request->price,
            'image'           => $this->saveImage($request->file('image'), 'Images/Extras', 400, 400),
            'ar' => [
                'name'        => $request->name_ar,
                'description' => $request->description_ar,
            ],
            'en' => [
                'name'        => $request->name_en,
                'description' => $request->description_en,
            ]
        ]);

        return $this->apiResponse(new ExtraResource($extra), 'The extra has been created successfully', 200);
    }

    public function update(UpdateExtraRequest $request, $id)
    {
        $resturant = $request->user('resturant_api');

        $extra = Extra::find($id);
        $extra->update([
            'resturant_id'    => $resturant->id,
            'price'           => $request->price,
            'image'           => $this->updateImage($extra, $request, 'image', 'Images/Extras', 400, 400),
            'ar' => [
                'name'        => $request->name_ar,
                'description' => $request->description_ar,
            ],
            'en' => [
                'name'        => $request->name_en,
                'description' => $request->description_en,
            ]
        ]);

        return $this->apiResponse(new ExtraResource($extra), 'The extra has been updated successfully', 200);
    }

    public function destroy($id)
    {
        $extra = Extra::find($id);
        (File::exists($extra->image)) ? File::delete($extra->image) : Null;
        $extra->delete();
        return $this->apiResponse(null, 'The extra has been deleted successfully', 200);
    }
}
