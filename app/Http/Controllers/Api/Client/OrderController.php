<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Client_App\Order\StoreOrderRequest;
use App\Http\Resources\Api\Client_App\Order\OrderResource;
use App\Http\Traits\ApiResponseTrait;
use App\Models\ExtraOrder;
use App\Models\MealOrder;
use App\Models\Order;
use App\Models\PackageOrder;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use  ApiResponseTrait;

    public function index(Request $request)
    {
        $client = $request->user('client_api');
        $orders = Order::where('client_id', $client->id)->orderBy('created_at', 'DESC')->paginate(20);
        return $this->apiResponse(OrderResource::collection($orders), 'All orders for this client ', 200);
    }
    public function store(StoreOrderRequest $request)
    {

        $client = $request->user('client_api');
        $order = Order::create([
            'client_id'                => $client->id,
            'resturant_id'             => $request->resturant_id,
            'representative_id'        => $request->representative_id,
            'address_id'               => $request->address_id,
            'coupon_id'                => $request->coupon_id,
            'payment_method'           => $request->payment_method,
            'delivery_commission'      => $request->delivery_commission,
            'order_details'            => $request->order_details,
            'total_price'              => $request->total_price,
            'has_discount'             => $request->has_discount,
            'discount_type'            => $request->discount_type,
            'discount_value'           => $request->discount_value,
            'total'                    => $request->total
        ]);



        $meals=$request->meals;
        if($meals){
            foreach ($meals as $meal){

                MealOrder::create([
                    'meal_id'           =>$meal['meal_id'],
                    'order_id'          =>$order->id,
                    'quantity'          =>$meal['quantity'],
                ]);
            }
        }

        $extras=$request->extras;
        if($extras){
            foreach ($extras as $extra){
                ExtraOrder::create([
                    'extra_id'          =>$extra['extra_id'],
                    'order_id'          =>$order->id,
                    'quantity'          =>$extra['quantity'],
                ]);
            }
        }

        $packages=$request->packages;
        if($packages){
            foreach ($packages as $package){
                PackageOrder::create([
                    'package_id'        =>$package['package_id'],
                    'order_id'          =>$order->id,
                    'quantity'          =>$package['quantity'],
                ]);
            }
        }


        return $this->apiResponse(new OrderResource($order), 'The order has been created successfully', 200);
    }



}
