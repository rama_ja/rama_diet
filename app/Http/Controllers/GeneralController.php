<?php

namespace App\Http\Controllers;

use App\Http\Resources\Api\General\AboutAppResource;
use App\Http\Resources\Api\General\TermsConditionsResource;
use App\Http\Traits\ApiResponseTrait;
use App\Models\AboutApp;
use App\Models\Terms;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    use ApiResponseTrait;
    public function getTermsAndConditions()
    {
        $terms_and_conditions = Terms::first();
        return $this->apiResponse(new TermsConditionsResource($terms_and_conditions), 'The Terms And Conditions for this App', 200);
    }

    public function getAboutApp()
    {
        $about_app = AboutApp::first();
        return $this->apiResponse(new AboutAppResource($about_app), 'The About App for this App', 200);
    }
}
