<?php

namespace App\Http\Resources\Api\Resturant_App\Representative;

use Illuminate\Http\Resources\Json\JsonResource;

class RepresentativeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'name'                => $this->name,
            'city_id'             => $this->city_id,
            'personal_image'      => $this->personal_image,
            'vehicle_type'        => $this->vehicle_type,
            'vehicle_brand'       => $this->vehicle_brand,
            'type'                => $this->type,
            'vehicle_model'       => $this->vehicle_model,
        ];

    }
}
