<?php

namespace App\Http\Resources\Api\Client_App\Resturant;

use Illuminate\Http\Resources\Json\JsonResource;

class ResturantRatingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'evaluation'   => $this->evaluation,
            'client_experience'=>$this->client_experience
        ];
    }
}
