<?php

namespace App\Http\Requests\Api\Client_App\Order;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id'                          => 'required',
            'representative_id'                  => 'required',
            'address_id'                         => 'required',
            'resturant_id'                       => 'required',
            'coupon_id'                          => 'nullable',
            'payment_method'                     => 'required|string',
            'delivery_commission'                => 'required|integer',
            'order_details'                      => 'required|string',
            'total_price'                        => 'required|integer',
            'has_discount'                       => 'required|integer',
            'discount_type'                      => 'nullable|integer',
            'discount_value'                     => 'nullable|integer',
            'total'                              => 'required|integer',
            'meals.*'                            => 'nullable',
//            'extras.*'                           => 'nullable|integer',
//            'packages.*'                         => 'nullable|integer',
        ];
    }
}
