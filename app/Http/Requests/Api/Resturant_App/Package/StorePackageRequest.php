<?php

namespace App\Http\Requests\Api\Resturant_App\Package;

use Illuminate\Foundation\Http\FormRequest;

class StorePackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ar'                              => 'required|string',
            'name_en'                              => 'required|string',
            'price'                                => 'required|numeric|gt:0',
            'description_ar'                       => 'required|string',
            'description_en'                       => 'required|string',
            'total_calories'                       => 'required|numeric',
            'carbohydrate'                         => 'required|numeric',
            'protein'                              => 'required|numeric',
            'fats'                                 => 'required|numeric',
            'main_image'                           => 'required|image|mimes:jpg,jpeg,png,gif,webp',
            'status'                               => 'required|integer',
            'images'                               => 'required|array|max:10',
            'images.*'                             => 'required|image|mimes:jpg,jpeg,png,gif,webp',
            'package_details.*.day_id'             => 'required|integer',
            'package_details.*.delivery_time'      => 'required',
            'package_details.*.description_ar'     => 'required|string',
            'package_details.*.description_en'     => 'required|string',
        ];
    }
}
