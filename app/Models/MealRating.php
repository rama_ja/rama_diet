<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MealRating extends Model
{
    use HasFactory;
    protected $fillable = ['meal_id', 'client_id','evaluation'];
}
