<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExtraOrder extends Model
{
    use HasFactory;
    protected $fillable = ['extra_id', 'order_id','quantity'];

    public function extra()
    {
        return $this->hasOne(Extra::class);
    }

}
