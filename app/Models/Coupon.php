<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;
    protected $fillable = ['resturant_id', 'code', 'minimum_price','discount_amount','discount_type','start_date','end_date'];

}
