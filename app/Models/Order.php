<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = ['client_id','representative_id','address_id','resturant_id','coupon_id','payment_method','delivery_commission',
        'order_details','total_price','has_discount','discount_type','discount_value','total'];


    public function representative()
    {
        return $this->belongsTo(Representative::class);
    }

    public function resturant()
    {
        return $this->belongsTo(Resturant::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }
////////////////////////////////////////
    public function meals()
    {
        return $this->belongsToMany(Meal::class,'meal_orders', 'order_id', 'meal_id');
    }

    public function packages()
    {
        return $this->belongsToMany(Package::class,'package_orders', 'order_id', 'package_id');
    }

    public function extras()
    {
        return $this->belongsToMany(Extra::class,'extra_orders', 'order_id', 'extra_id');
    }



}
