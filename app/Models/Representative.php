<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Representative extends Model
{
    use HasFactory;
    protected $fillable = ['resturant_id','name', 'email', 'mobile', 'city_id', 'personal_image', 'vehicle_type', 'vehicle_brand', 'type','vehicle_model','license_image','form_image'];


}
