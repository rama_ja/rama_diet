<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepresentativesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representatives', function (Blueprint $table) {
            $table->id();
            $table->foreignId('resturant_id')->nullable()->constrained('resturants')->onDelete('cascade');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('mobile')->unique();
            $table->foreignId('city_id')->constrained('cities')->onDelete('cascade');
            $table->string('personal_image')->default('');

            $table->string('vehicle_type');
            $table->string('vehicle_brand');
            $table->string('type');
            $table->string('vehicle_model');
            $table->string('license_image');
            $table->string('form_image');
            $table->boolean('status')->comment('1:accepted, 2:not accepted')->default('1');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('representatives');
    }
}
